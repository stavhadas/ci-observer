const content = require('fs').readFileSync(__dirname + '/../index.html', 'utf8');
const groups = require('fs').readFileSync(__dirname + '/../groups.json', 'utf8');
const http = require('http');
const https = require('https')
const Events = require('events');
const express = require('express');
const request = require('sync-request')
var app = express ();
var server = http.createServer (app);
// app.engine('.html', require('ejs').__express);

app.get ('/', function (req, res){
    res.setHeader('Content-Type', 'text/html');
    res.setHeader('Content-Length', Buffer.byteLength(content));
    res.end(content);
});
app.use(express.static(__dirname + '/../resources'));
const io = require('socket.io')(server);

io.on('connection', socket => {
  socket.on('/pipelines', (callback)=>{
    get_subprojects(7519071, (projects)=>{
        get_pipelines(projects, (pipelines)=>{
            callback(pipelines)
        });
    });
  });
});

server.listen(8080);


function get_subprojects(group_id, callback){
    https.get('https://gitlab.com/api/v4/groups/' + group_id + '/projects', (resp) => {
        let data = ''
        resp.on('data', (this_data)=>{
            data += this_data
        });

        resp.on('end', () => {
            callback(JSON.parse(data))
        });
      });

      
    // https.get('https://gitlab.com/api/v4/groups/' + group_id + '/subgroups', (resp) => {
    //     let data = ''
    //     resp.on('data', (this_data)=>{
    //         data += this_data
    //     });

    //     resp.on('end', () => {
    //         let subgroups = JSON.parse(data)
    //     });
    //   }).on('error', (err)=>{
    //   });
}

function get_pipelines(projects, callback){
    let master_pipelines = []
    const events = new Events;
    master_pipelines = []
    for (i = 0; i < projects.length ; i++){
        var res = request('GET', 'https://gitlab.com/api/v4/projects/' + projects[i].id + '/pipelines');
        let pipelines = JSON.parse(res.getBody('utf8'));
        // Delete pipelines not for master
        for (j = 0; j < pipelines.length ; j++){
            if (pipelines[j].ref != 'master')
            {
                delete pipelines[j]
            }
        }
            
        // Sort pipelines by date
        pipelines.sort((firstEl, secondEl)=>{
            return secondEl.updated_at.localeCompare(firstEl.updated_at);
        });
        master_pipelines.push({"project_name" : projects[i].name,
                               "avatar" : projects[i].avatar_url,
                               "pipeline": pipelines[0]})
    }
    callback(master_pipelines)
}